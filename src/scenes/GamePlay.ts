import 'phaser';
import CONFIG from "../../config";

let gameOptions = {
    playerSize: 128,
    tunnelLengthRange: [400, 600],
    playerSpeed: 250,
    playerSpeedIncrement: 2,
    localStorageName: "turnScore2"
}

const CLOCKWISE = 0;
const COUNTERCLOCKWISE = 1;
const UP = 0;
const RIGHT = 1;
const DOWN = 2;
const LEFT = 3;
const WATING = 0;
const PLAY = 1;

export default class GamePlay extends Phaser.Scene {
    currentSpeed : any;
    tunnels : any;
    tunnelToWatch : number;
    player : any;
    bestScore: number;
    score: number;
    logo: Phaser.GameObjects.Sprite;
    scoreText: Phaser.GameObjects.BitmapText;
    bestScoreText: Phaser.GameObjects.BitmapText;
    gameState: number;
    playButton: Phaser.GameObjects.Sprite;

    constructor() {
        super("GamePlay");
		this.gameState = WATING;
    }
    preload() {
        this.load.image("hole", "assets/sprites/hole.png");
        this.load.image("player", "assets/sprites/player.png");

		this.load.image("gameOver", "assets/sprites/GameOver.png");
		this.load.image("logo", "assets/sprites/Banner_hifpt.png");
        this.load.image("play", "assets/sprites/play.png");
        this.load.image("scorepanel", "assets/sprites/scorepanel.png");
        this.load.image("scorelabels", "assets/sprites/scorelabels.png");
        this.load.bitmapFont("font", "assets/fonts/font.png", "assets/fonts/font.fnt");
    }
    create() {
        this.score = 0;
        this.bestScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : Number(localStorage.getItem(gameOptions.localStorageName));

        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height -10, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(1);

        let x = 150
        let y = 10
        var scorelabels = this.add.sprite(x, y, "scorelabels");
        scorelabels.setOrigin(0,0);
        scorelabels.setScale(0.8);
        var scorepanel = this.add.sprite(x, y+30, "scorepanel");
        scorepanel.setOrigin(0,0);
        scorepanel.setScale(0.8);
        this.scoreText = this.add.bitmapText( x+15, y+50, "font", "0", 50);
        this.scoreText.setOrigin(0, 0);
        this.bestScoreText = this.add.bitmapText( x+290, y+50, "font", this.bestScore.toString(), 50);
        this.bestScoreText.setOrigin(0, 0);

        if(this.gameState == PLAY){
            this.currentSpeed = gameOptions.playerSpeed;
            this.tunnels = [
                this.add.sprite(0, 0, "hole"),
                this.add.sprite(0, 0, "hole"),
                this.add.sprite(0, 0, "hole")
            ];
            this.tunnelToWatch = 0;
            this.player = this.add.sprite(this.scale.width / 2, this.scale.height / 2, "player");
            this.player.displayWidth = gameOptions.playerSize;
            this.player.displayHeight = gameOptions.playerSize;
            this.player.setDepth(1);
            this.tunnels[0].x = this.scale.width / 2,
            this.tunnels[0].y = this.player.getBounds().bottom;
            this.tunnels[0].setOrigin(0.5, 1);
            this.tunnels[0].direction = UP;
            this.tunnels[0].displayWidth = this.player.displayWidth;
            this.tunnels[0].displayHeight = gameOptions.tunnelLengthRange[1];
            this.input.on("pointerdown", this.changeDirection, this);
            this.attachTunnel(0);
            this.attachTunnel(1);
            this.playButton.setVisible(false);
        }
        else{
            var text = this.add.text(this.scale.width/2 , this.scale.height/4, "Welcome to", {
				font: "bold 70px Arial",
                color: "#6e43d9"
			});
			text.setOrigin(0.5, 1);
            var text = this.add.text(this.scale.width/2 , this.scale.height/4+100, "TURN Game", {
				font: "bold 70px Arial",
                color: "#6e43d9"
			});
            text.setOrigin(0.5, 1);
            
			this.playButton = this.add.sprite(this.scale.width/2, this.scale.height/2, "play");
			this.playButton.setScale(1)
			this.playButton.setOrigin(0.5,0.5);
			this.playButton.setInteractive();
			this.playButton.on("pointerdown", function(){
				this.scene.start("GamePlay");
				this.gameState = PLAY;
			}, this)
		}
        
    }

    attachTunnel(n) {
        let nextDirection = Phaser.Math.Between(CLOCKWISE, COUNTERCLOCKWISE);
        let nextLength = Phaser.Math.Between(gameOptions.tunnelLengthRange[0], gameOptions.tunnelLengthRange[1]);
        let bounds = this.tunnels[n].getBounds();
        let tunnelIndex = (n + 1) % 3;
        switch(this.tunnels[n].direction) {
            case UP: {
                this.tunnels[tunnelIndex].x = (nextDirection == CLOCKWISE) ? bounds.left : bounds.right;
                this.tunnels[tunnelIndex].y = bounds.top;
                this.tunnels[tunnelIndex].displayWidth = nextLength;
                this.tunnels[tunnelIndex].displayHeight = this.player.displayHeight;
                this.tunnels[tunnelIndex].direction = (nextDirection == CLOCKWISE) ? RIGHT : LEFT;
                this.tunnels[tunnelIndex].setOrigin((nextDirection == CLOCKWISE) ? 0 : 1, 0);
                break;
            }
            case LEFT: {
                this.tunnels[tunnelIndex].x = bounds.left;
                this.tunnels[tunnelIndex].y = (nextDirection == CLOCKWISE) ? bounds.bottom : bounds.top;
                this.tunnels[tunnelIndex].displayWidth = this.player.displayWidth;
                this.tunnels[tunnelIndex].displayHeight = nextLength;
                this.tunnels[tunnelIndex].direction = (nextDirection == CLOCKWISE) ? UP : DOWN;
                this.tunnels[tunnelIndex].setOrigin(0, (nextDirection == CLOCKWISE) ? 1 : 0);
                break;
            }
            case RIGHT: {
                this.tunnels[tunnelIndex].x = bounds.right;
                this.tunnels[tunnelIndex].y = (nextDirection == CLOCKWISE) ? bounds.top : bounds.bottom;
                this.tunnels[tunnelIndex].displayWidth = this.player.displayWidth;
                this.tunnels[tunnelIndex].displayHeight = nextLength;
                this.tunnels[tunnelIndex].direction = (nextDirection == CLOCKWISE) ? DOWN : UP;
                this.tunnels[tunnelIndex].setOrigin(1, (nextDirection == CLOCKWISE) ? 0 : 1);
                break;
            }
            case DOWN: {
                this.tunnels[tunnelIndex].x = (nextDirection == CLOCKWISE) ? bounds.right : bounds.left;
                this.tunnels[tunnelIndex].y = bounds.bottom;
                this.tunnels[tunnelIndex].displayWidth = nextLength;
                this.tunnels[tunnelIndex].displayHeight = this.player.displayHeight;
                this.tunnels[tunnelIndex].direction = (nextDirection == CLOCKWISE) ? LEFT : RIGHT;
                this.tunnels[tunnelIndex].setOrigin((nextDirection == CLOCKWISE) ? 1 : 0, 1);
                break;
            }
        }
    }

    update(t, dt) {
        if(this.gameState == PLAY){
            this.scrollTunnels(this.tunnels[this.tunnelToWatch % 3].direction, this.currentSpeed * dt / 1000);
            this.updateScore();
            switch(this.tunnels[this.tunnelToWatch % 3].direction) {
                case UP: {
                    if(this.player.getBounds().bottom <= this.tunnels[this.tunnelToWatch % 3].getBounds().top) {
                        this.gameOver();
                    }
                    break;
                }
                case DOWN: {
                    if(this.player.getBounds().top >= this.tunnels[this.tunnelToWatch % 3].getBounds().bottom) {
                        this.gameOver();
                    }
                    break;
                }
                case LEFT: {
                    if(this.player.getBounds().right <= this.tunnels[this.tunnelToWatch % 3].getBounds().left) {
                        this.gameOver();
                    }
                    break;
                }
                case RIGHT: {
                    if(this.player.getBounds().left >= this.tunnels[this.tunnelToWatch % 3].getBounds().right) {
                        this.gameOver();
                    }
                    break;
                }
            }
        }
    }

    updateScore(){
        this.score++;
        if(this.score > this.bestScore){
            this.bestScore = this.score;
            this.bestScoreText.text = this.bestScore.toString();
        }
        this.scoreText.text = this.score.toString();
    }
    scrollTunnels(dir, n) {
        for(let i = 0; i < this.tunnels.length; i ++) {
            this.tunnels[i].x += (dir == LEFT) ? n : (dir == RIGHT ? -n : 0);
            this.tunnels[i].y += (dir == UP) ? n : (dir == DOWN ? -n : 0);
        }
    }

    changeDirection() {
        if(this.gameState == PLAY){
            let currentTunnel = this.tunnels[this.tunnelToWatch % 3];
            let nextTunnelBounds = this.tunnels[(this.tunnelToWatch + 1) % 3].getBounds();
            switch(currentTunnel.direction) {
                case UP:
                case DOWN: {
                    let difference = (currentTunnel.direction == UP) ? (this.player.getBounds().top - currentTunnel.getBounds().top) : (this.player.getBounds().bottom - currentTunnel.getBounds().bottom);
                    if(this.player.displayHeight <= Math.abs(difference)) {
                        this.gameOver();
                    }
                    this.player.displayHeight -= Math.abs(difference);
                    if(difference > 0) {
                        this.player.y = nextTunnelBounds.bottom - this.player.displayHeight / 2;
                    }
                    else {
                        this.player.y = nextTunnelBounds.top + this.player.displayHeight / 2;
                    }
                    break;
                }
                case LEFT:
                case RIGHT: {
                    let difference = (currentTunnel.direction == LEFT) ? (this.player.getBounds().left - currentTunnel.getBounds().left) : (this.player.getBounds().right - currentTunnel.getBounds().right);
                    if(this.player.displayWidth <= Math.abs(difference)) {
                        this.gameOver();
                    }
                    this.player.displayWidth -= Math.abs(difference);
                    if(difference > 0) {
                        this.player.x = nextTunnelBounds.right - this.player.displayWidth / 2;
                    }
                    else {
                        this.player.x = nextTunnelBounds.left + this.player.displayWidth / 2;
                    }
                }
                break;
            }
            this.tunnelToWatch ++;
            this.attachTunnel((this.tunnelToWatch + 1) % 3);
            this.currentSpeed += gameOptions.playerSpeedIncrement;    
        }
    }

    gameOver() {
        this.cameras.main.shake(800, 0.01);

        this.gameState = WATING;
        this.player.destroy();
        
        localStorage.setItem(gameOptions.localStorageName, this.bestScore.toString());
        var gameOver = this.add.sprite(this.scale.width/2, this.scale.height/2-200, "gameOver");
        gameOver.setScale(1)
        gameOver.setOrigin(0.5,0.5);
        this.time.addEvent({
            delay: 4000,
            callbackScope: this,
            callback: function(){
		        this.scene.start("GamePlay");
            }
        })
    }
}


